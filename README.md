#README#
Enllaç al manual:[manual](https://www.digitalocean.com/community/tutorials/how-to-deploy-a-symfony-application-to-production-on-ubuntu-14-04)

1. En el Step 1: al instalar php el comando es incorrecto, correcta: sudo apt-get install git php7.0-cli php7.0-curl acl.
2. En el Step 2, la ruta del es incorrecta, correcta: sudo nano /etc/mysql/mysql.conf.d/mysqld.conf.
Al modificar el texto el rojo, hay que tenerlo en cuenta para modificarlo mas tarde.
3. En el Step 6, la ruta del archivo es incorrecta, correcta: sudo nano /etc/php/7.0/apache2/php.ini. Cuanto hay que modificar la zona horaria, utilizar: date.timezone = Europe/Madrid.

![To Do Funciona](todo.png)